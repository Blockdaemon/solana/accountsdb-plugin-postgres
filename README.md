# solana-accountsdb-plugin-postgres

This repository provides build artifacts of solana-accountsdb-plugin-postgres.
Source: https://github.com/solana-labs/solana/tree/master/accountsdb-plugin-postgres

## Workflow

The workflow mirrors Solana's [release process](https://github.com/solana-labs/solana/blob/master/RELEASE.md).

### Releases

Each tagged Solana release results in a plugin tag.

The tag naming scheme follows `<solana_version>-<revision>` where
  - `solana_version` is the equivalent tag name in the Solana repo;
  - `revision` is the revision number for a release targeting the same Solana tag.
    The revision is used to publish minor updates / bug fixes.

Example tag name: `v1.9.4-1` for Solana version `v1.9.4`, dist revision `1`.

### Branches

Supported Solana release branches are the preferred versions on devnet, testnet, and mainnet-beta.

This repo's branches will mirror Solana release branches. e.g. branch names `v1.8` and `v1.9`.

### Solana patch version bump

> Pro-tip: The GitLab Web IDE is nice.

1. Check out minor version branch, e.g. `git checkout v1.8`.
2. Update `SOLANA_VERSION` in [`.gitlab-ci.yml`](./.gitlab-ci.yml).
3. Send a MR titled `[JIRA_TICKET_ID] Solana v1.8.12`.
   Set branch name to ticket ID.
   Replace Jira ticket ID with the equivalent ticket in the Protocols (PRO) board.
4. Get MR approved and merged.
5. Release tag according to naming scheme documented above.

### Solana minor version bump

1. Start a new minor version branch, e.g. `git checkout -b v1.10`.
2. Set new branch as the default branch.
3. Follow "patch version bump steps".

## CI

GitLab CI will automatically build and publish artifacts on tags.

Please do not manually push release artifacts for security reasons.
